# Rtv2

Version améliorée du raytracer basique comportant plus de formes et d'effets visuels. Une interface permettant des modifications en direct a été implementeé grâce à GTK.

Nous étions 4 à participer à la réalisation du projet.

* ### reflection
![raytracer in action](ressources/saves/reflection.jpeg)

* ### mirrors_edge
![raytracer in action](ressources/saves/mirrors_edge.jpeg)

* ### fox
![raytracer in action](ressources/saves/fox.jpeg)

* ### cut_objects
![raytracer in action](ressources/saves/cut_objects.jpeg)

* More
![raytracer in action](ressources/saves/nmap.png)

![raytracer in action](ressources/saves/edge2.png)

![raytracer in action](ressources/saves/empty.png)